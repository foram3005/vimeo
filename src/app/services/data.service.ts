import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http'

@Injectable()
export class DataService {

  constructor(public http: HttpClient) { }

  getJSON(){
    return this.http.get('./assets/query.json')
  }
}
