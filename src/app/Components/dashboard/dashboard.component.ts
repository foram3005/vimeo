import { Component, OnInit } from '@angular/core';
import {HttpClient} from '@angular/common/http'

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  shotsList:any

  constructor(public http: HttpClient) { }

  ngOnInit() {
    this.shotsList = [1,2,3,4,5,6,7,8,9,10]
    this.populateData()
  }

  getJsonData(){
    return this.http.get('../../../assets/query.json')
  }

  populateData() {
    this.getJsonData().subscribe(results => {
      console.log('results',results)
      this.shotsList = results;
      console.log('results',this.shotsList.hits.hits[0]._source.thumbnail)
    })
    //console.log(x)
  }

}