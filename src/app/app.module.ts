import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HttpClientModule} from '@angular/common/http'

import { Routings } from './app.routing';

import { AppComponent } from './app.component';
import {ClarityModule} from '@clr/angular';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { ShotsComponent } from './components/shots/shots.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';

import { DashboardService } from './services/dashboard.service'


@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    ShotsComponent,
    HeaderComponent,
    FooterComponent
  ],
  imports: [
    BrowserModule,
    Routings,
    HttpClientModule,
    ClarityModule
  ],
  providers: [DashboardService],
  bootstrap: [AppComponent]
})
export class AppModule { }
